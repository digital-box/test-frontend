# Frontend Test

 - Fork this repository.
 - Create the page found in [psd/chopsuey.psd](psd/chopsuey.psd).
 - Push your changes to your forked repository.

You may use any tool you're comfortable with including preprocessors,
build systems, existing grid systems or frameworks.

Don't forget to add any installation/setup instructions with your
completed implementation of the page.

You'll need the font Lato which is included in this repository for you
under [resources/fonts/](resources/fonts/).

N.B. If you're familiar with various preprocessors, we're partial to
SASS so ideally build it using that.

## Bonus points

 - Consider ARIA